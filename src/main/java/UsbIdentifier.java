
class UsbIdentifier {

    private String path;

    private UsbDevice usbDevice;


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public UsbDevice getUsbDevice() {
        return usbDevice;
    }

    public void setUsbDevice(UsbDevice usbDevice) {
        this.usbDevice = usbDevice;
    }
}
