import java.util.List;

class UsbDevice {

    private String bus;

    private String port;

    private String deviceClass;

    private List<UsbDevice> childrenList;

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceClass(String deviceClass) {
        this.deviceClass = deviceClass;
    }

    public List<UsbDevice> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<UsbDevice> childrenList) {
        this.childrenList = childrenList;
    }
}
