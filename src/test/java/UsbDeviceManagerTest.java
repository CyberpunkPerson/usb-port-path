import org.junit.Test;

import java.text.ParseException;

public class UsbDeviceManagerTest {

    @Test(expected = ParseException.class)
    public void throwExceptionWhenWrongInputPass() throws ParseException {
        UsbDeviceManager usbDeviceManager = UsbDeviceManager.getInstance();
        usbDeviceManager.getUSBIdentifier("Dev 6g6");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionWhenDeviceWasNotFound() throws ParseException {
        UsbDeviceManager usbDeviceManager = UsbDeviceManager.getInstance();
        usbDeviceManager.getUSBIdentifier("Dev 65333");
    }
}
